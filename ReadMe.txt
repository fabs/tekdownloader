ReadMe

1.	General Information 
2.	Installation 
3.	Configuration tips >>tekdownloader.sh<<	


####################################################

1.	General Information
-------------------------

-- 	This script allows you to download audio files (.mp3s) of the following youtube podcasts.
	#	"The Tek" by TekSyndicate (https://www.youtube.com/user/razethew0rld)
	#	"WASD" by TekSyndicate (https://www.youtube.com/user/razethew0rld)
	#	"INBOX" by TekSyndicate (https://www.youtube.com/user/razethew0rld)
	#	"The WAN Show" by LinusTechTips (https://www.youtube.com/user/LinusTechTips)
	#	"Tech Talk" by JayzTwoCents (https://www.youtube.com/user/Jayztwocents)
	
--	This script is compatible and tested on following distros:
	*	xUbuntu 14.04 
	*	Ubuntu 14.04 Server
	
--	There is a high chance that this script will work on newer versions of ubuntu too. Maybe older versions are incompatible due to older/incompatible versions
	of the dependencies.

--	Maybe other distros will work too (Debian....). But keep in mind, that this script is written for the "dash shell". 

--	This script is completely written for cronjob use.
	
--	All mp3s will be id3tagged.
	
--	Ability to upload all mp3 files to your dropbox

-- 	Is able to send a summary of (automatically) downloaded podcasts via mail.

--	Is able to send pusbullet notifications to one of your pushbullet channels, when a new podcast is available.

--	You can configure the bitrate of the the MP3 files.



2.	Installation
-------------------

A.	Install the following packets:
	curl
	eyeD3
	rsstail 
	youtube-dl 
	unzip 
	eyeD3
	

B.	Download the project:
	git clone https://gitlab.com/fabs/tekdownloader.git

	
C.	Check permissions. If you don't have executable permissions.
	chmod +x tekdownloader.sh 
	
D.	Configure the script. See point 3 for further information.


E.	Run the script for the first time


F.	Edit your cronjob configuration


3.	Configuration tips >>tekdownloader.sh<<
----------------------------------------------

--	On top of the script is a "config area". You have to configure it to your needs.

--	At least you have to configure the variables "mp3p" and "d". The path "mp3p" will be used to store your mp3s. The path "d" is used as a "database" for the script. 
	
--	If you delete "mp3p" nothing will happen.

--	If you delete "d" it will start to download everything again. It will also interfere with your already downloaded files. In this case delete files in "mp3p" and "d".

--	Every configuration option is commented. 

--	You can enable/disable podcasts whenever you want

--	By default it will start to download all files, which are available via the RSS-Feed of the youtube channel. So the script might download a few podcasts, when you launch it for first time.
	Check the variable "downall" to configure that.
	
--	If you know what you are doing. Check the "Advanced Config Area". 


Have fun...



________________					
< tekdownloader >
 ---------------
   \
    \
        .--.
       |o_o |
       |:_/ |
      //   \ \
     (|     | )
    /'\_   _/`\
    \___)=(___/