#!/bin/sh
#
# tekdownloader (Version 1.1 / November 2015)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#


#####################################################################################################


######################################
#############CONFIG AREA##############
######################################

#-----IMPORTANT CONFIG

# Path where mp3-file will be stored (Always absolute path /home/user/etc....)
mp3p="/configure/that"

# Location to save work data (Never delete this folder or the stuff in it)
d="/configure/that/too"

# 1 = enabled
# 0 = disabled 

# Download Tek Syndicate: >>The Tek<<
T=1

# Download Tek Syndicate: >>WASD<<
WASD=0

#Download #INBOX
INBOX=0

# Download LinusTechTips: >>The Wan Show<<
LTT=0

# Download JayzTwoCents & Barnaguydude: >>Tech Talk<<
JBTT=0


# Attention this setting will download all Podcasts, which are available via RSS. 
# If you turn this setting off, it won't download anything. But it will download new podcasts, when they are available.
downall="1"

#--------------------
######################################
####ADVANCED CONFIG / EXPERIMENTAL####
######################################

# Configure the Bitrate of mp3-File
bitr="128k"

# RSS Base URL Tek Syndicate 
URLT="https://www.youtube.com/feeds/videos.xml?user=razethew0rld"

# RSS Base URL LinusTechTips
URLLTT="https://www.youtube.com/feeds/videos.xml?user=LinusTechTips"

# RSS Base URL JayzTwoCents
JBTTURL="https://www.youtube.com/feeds/videos.xml?user=Jayztwocents"

# Dependencies
dep="rsstail youtube-dl unzip eyeD3"

# Turn on Mailing. Check end of script for further configuration. 
mailing=0

# Define email recipient
recipient=mail@example.com

# Dropbox Upload. This needs a seperate Tool. You can download it from here:
# https://github.com/andreafabrizi/Dropbox-Uploader 
# This tools has to be installed and working correctly.
dropup=0

# Path where Dropbox-Uploader is installed 
dropupp="/path/to/Dropbox-Uploader"

# Upload folder on your dropbox
dropfolder="tekdownloader"

# only for debug purposes --> enabled = 1 / disabled = 0
DEBUG=0

#Enable Pushbullet 
pushbullet=0

#Pushbullet Access Token
pushtoken=pasteYourTokenHere

#Pushbullet extra line
extrapush="Extra text"

#Define the channel for pushbullet
pushchannel=channelforPUSHBULLET


#----------------END OF CONFIG AREA-------------

###################################################################################################################

#----------------FUNCTIONS----------------------

#Downloads from youtube. Create ID3-Tag, download thumbnail. 
downloader()
{
        while read -r zeile
        do
        	title=$(cat $d/"$nowd"feed.txt | grep -B1 $zeile | grep Title |  sed s/'Title: '//g | sed s/' '/_/g | tr -dc '[[:alnum:]]._-')
	        artist=$(cat $d/"$nowd"feed.txt | grep -A2 $zeile | grep 'Author' | sed s/'Author: '//g)
	        year=$(cat $d/"$nowd"feed.txt | grep -A1 $zeile | grep Pub.date | cut -d ' ' -f5)
	        id=$(echo "$zeile" | sed s/.*=//g)
	        wget -q http://img.youtube.com/vi/$id/maxresdefault.jpg -O $d/$title.jpg
	        youtube-dl -q -x --output $mp3p/$nowd/$title".%(ext)s" --audio-format mp3 --audio-quality $bitr $zeile 
		
		if [ $DEBUG -eq 1 ];
		then 
	        	eyeD3 --force-update --no-color --artist "$artist" --title "$title" --genre "Podcast" -Y "$year" --comment "Downloaded with unicorn power!" --add-image "$d/$title.jpg:FRONT_COVER:front cover" $mp3p/$nowd/$title.mp3 
			echo $id
		        echo "$mp3p/$nowd/$title.mp3"
		else
	        	eyeD3 --force-update --no-color --artist "$artist" --title "$title" --genre "Podcast" -Y "$year" --comment "Downloaded with unicorn power!" --add-image "$d/$title.jpg:FRONT_COVER:front cover" $mp3p/$nowd/$title.mp3 > /dev/null 2>&1
		fi
		
		if [ $dropup -eq 1 ];
		then 
			$dropupp/dropbox_uploader.sh -q mkdir /$dropfolder/$nowd
			$dropupp/dropbox_uploader.sh -q upload $mp3p/$nowd/$title.mp3 /$dropfolder/$nowd
		fi

		rm $d/$title.jpg
        done < $d/download.txt

		cp $d/"$nowd"urls.txt $d/"$nowd"urlscompare.txt
	
		mailf
		pushb
	
		#Clear download.txt
		if [ -f $d/download.txt ];
		then 
			rm $d/download.txt 
			touch $d/download.txt
		fi

}

#This function checks if >>compare-file<< is available. If downall variable is set to 1, the script begins to download all podcasts available (only first start).
comparefunction()
{
        #Check for "compare file"
        if [ ! -f $d/"$nowd"urlscompare.txt ];
        then
                if [ $downall -eq 1 ];
                then
                       touch $d/"$nowd"urlscompare.txt
                else 
                       #cp $d/$(nowd)urls.txt $d/$(nowd)urlscompare.txt
                       cp $d/"$nowd"urls.txt $d/"$nowd"urlscompare.txt
                fi
        fi 
}

#Creates Download List --> All links in this file will be downloaded
todownload()
{
	sdiff -d $d/"$nowd"urls.txt $d/"$nowd"urlscompare.txt | grep '<' | sed s/'<'//g >> $d/download.txt
}

#Mailfunction
mailf()
{
	if [ $mailing -eq 1 ];
	then 
	
	while read -r line2
        do
			youtube-dl $line2 --get-filename >> $d/mail.txt
			echo "\n" >> $d/mail.txt
			youtube-dl $line2 --get-duration >> $d/mail.txt
			youtube-dl $line2 --get-description >> $d/mail.txt
			echo "\n" >> $d/mail.txt
			echo "--------------------------" >> $d/mail.txt
        done < $d/download.txt
        
	fi
}


#Pushbullet function
pushb()
{
	if [ $pushbullet -eq 1 ];
	then
		while read -r line3
		do
			pusht=$(youtube-dl $line3 --get-filename)
			echo "Description:\n--------------------\n" > $d/pushl.txt 
			youtube-dl $line3 --get-description >> $d/pushl.txt 
			echo "\n--------------------\n" >> $d/pushl.txt
			youtube-dl $line3 --get-duration >> $d/pushl.txt 
			echo "$extrapush" >> $d/pushl.txt 
			pushbt=`cat $d/pushl.txt`
			curl -s -u $pushtoken: https://api.pushbullet.com/v2/pushes -d type=note -d channel_tag="$pushchannel" -d title="$pusht" -d body="$pushbt" -X POST > /dev/null
		done < $d/download.txt
	fi
}
			



#-----------------------------------------------

#Check installed packages rsstail, youtube-dl
for i in $dep
do
	command -v $i >/dev/null && continue 
	echo "\nPlease install >> $dep <<. \nSomething is missing. Please check.\n"
	exit 0
done

#Check if Base-Folders are existing
if [ ! -d "$mp3p" ];
then 
	echo "Check Variable >>mp3p<< ! There might be something wrong. Folder is not existing"
	exit 0
fi

if [ ! -d "$d" ];
then 
	echo "Check Variable >>d<< ! There might be something wrong. Folder is not existing"
	exit 0
fi

if [ $mailing -eq 1 ];
then 
	echo "New Podcasts available! \n\n" > $d/mail.txt
fi
	
#Check Dropbox Uploader availability
if [ $dropup -eq 1 ];
then 
	if [ ! -f $dropupp/dropbox_uploader.sh ];
	then 
			echo "There is no dropbox_uploader.sh. So Dropbox-Uploader might not be installed. Fix it or disable it."
			exit 0
	else
		$dropupp/dropbox_uploader.sh -q list /$dropfolder/ > $d/dropfolders.txt
	fi 
fi	

#The Tek loop
if [ $T -eq 1 ];
then
	nowd="The_Tek"
	rsstail -1 -a -d -p -l -u $URLT | grep -A3 'The Tek' > $d/"$nowd"feed.txt && grep 'Link' $d/"$nowd"feed.txt | sed s/'Link: '//g > $d/"$nowd"urls.txt
	comparefunction
	todownload
	downloader 
fi

#The WAN Show loop
if [ $LTT -eq 1 ];
then
        nowd="The_WAN"
        rsstail -1 -a -d -p -l -u $URLLTT | grep -A3 'WAN Show' > $d/"$nowd"feed.txt && grep 'Link' $d/"$nowd"feed.txt | sed s/'Link: '//g > $d/"$nowd"urls.txt
        comparefunction
        todownload
        downloader
fi

#Tech Talk loop
if [ $JBTT -eq 1 ];
then
        nowd="Tech_Talk"
        rsstail -1 -a -d -p -l -u $JBTTURL | grep -A3 'Talk #' > $d/"$nowd"feed.txt && grep 'Link' $d/"$nowd"feed.txt | sed s/'Link: '//g > $d/"$nowd"urls.txt
        comparefunction
        todownload
        downloader
fi

#INBOX Loop 
if [ $INBOX -eq 1 ];
then
        nowd="INBOX"
        rsstail -1 -a -d -p -l -u $URLT  | grep -A3 -i 'inbox.exe' > $d/"$nowd"feed.txt && grep 'Link' $d/"$nowd"feed.txt | sed s/'Link: '//g > $d/"$nowd"urls.txt
        comparefunction
        todownload
        downloader
fi

#WASD Loop 
if [ $WASD -eq 1 ];
then
        nowd="WASD"
        rsstail -1 -a -d -p -l -u $URLT  | grep -A3 'WASD' > $d/"$nowd"feed.txt && grep 'Link' $d/"$nowd"feed.txt | sed s/'Link: '//g > $d/"$nowd"urls.txt
        comparefunction
        todownload
        downloader
fi


#I know. Not the best solution...
if [ -f $d/mail.txt ];
then 
if grep -q http $d/mail.txt; then 
	echo "Size of your download folder:"  >> $d/mail.txt
    du -hs $mp3p >> $d/mail.txt
	
	#If necessary change this line to your environment
	mail -s "New Podcasts available" $recipient < $d/mail.txt
fi
fi

exit 0
	
